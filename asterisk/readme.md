### Instructions
The files with ".conf" extension are specific to asterisk and would form the basis
for the configuration. Feel free to modify these files to suite your scenario.
Moreso, you can override other asterisk configurations by adding them to the asterisk 
directory on build. This way the docker build command will copy over the configurations
to the image at build time

### build
docker build --tag="image-name" .

### launch
docker run -d --name "container-name" --net=host "image-name"